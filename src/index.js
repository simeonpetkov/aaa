import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { createFirestoreInstance, firestoreReducer } from "redux-firestore";
import {
  ReactReduxFirebaseProvider,
  firebaseReducer
} from "react-redux-firebase";
import { DB_CONFIG } from "./config";
import firebase from "firebase";
import { reducer as formReducer } from "redux-form";

firebase.initializeApp(DB_CONFIG);
firebase.firestore();
const db = firebase.firestore();

export const rootReducer = combineReducers({
  form: formReducer,
  firesore: firestoreReducer,
  firebase: firebaseReducer
});

const store = createStore(
  rootReducer,
  compose(applyMiddleware(thunk.withExtraArgument(db)))
);

const rrfProps = {
  firebase,
  config: { restaurants: "restaurants", reservations: "reservations" },
  dispatch: store.dispatch,
  createFirestoreInstance // <- needed if using firestore
};

ReactDOM.render(
  <Provider store={store}>
    <ReactReduxFirebaseProvider {...rrfProps}>
      <App />
    </ReactReduxFirebaseProvider>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
