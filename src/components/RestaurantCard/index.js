import React from "react";
import ImageList from "./image-list";

const RestaurantCard = props => {
  const { data, onOpenForm, user } = props;
  return (
    <div className="c-card">
      <div className="c-card-header">
        <img
          width={58}
          height={40}
          src={data.logo}
          style={{
            marginRight: 10
          }}
        />
        <h5>{data.name}</h5>
      </div>
      {!user && <ImageList images={data.images} stars={data.stars} />}
      {!user && <div className="c-description">{data.description}</div>}
      {user && (
        <>
          <div className="c-card-row">
            <p>Names: {user.names}</p>
          </div>
          <div className="c-card-row">
            <p>Phone number: {user.phone}</p>
          </div>
          <div className="c-card-row">
            <p>Time: {user.time}</p>
          </div>
          <div className="c-card-row" style={{ marginBottom: 16 }}>
            <p>Table location: {user.place}</p>
          </div>
        </>
      )}
      <button className="c-button--primary" onClick={() => onOpenForm(data)}>
        {user ? "Reserve again" : "Reserve"}
      </button>
    </div>
  );
};

export default RestaurantCard;
