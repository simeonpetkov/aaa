import React from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Stars from "simple-rating-stars";

const ImageList = props => {
  const { images, stars } = props;
  return (
    <div
      style={{
        borderRadius: 3,
        overflow: "hidden",
        marginBottom: 18,
        position: "relative"
      }}
    >
      <div className="o-stars-wrapper">
        <Stars
          stars={stars}
          outOf={5}
          full={"rgb(255, 241, 117)"}
          empty={"rgb(158, 158, 158)"}
          stroke={"#369"}
        />
      </div>
      <Carousel showThumbs={false} infiniteLoop autoPlay interval={8000}>
        {images.map((image, index) => {
          return <img key={index} src={image} alt={"img"} height={"190px"} />;
        })}
      </Carousel>
    </div>
  );
};

export default ImageList;
