import React from "react";
import { useSelector } from "react-redux";

import { Container, Row, Col } from "react-grid-system";
import RestaurantCard from "../RestaurantCard/index";
import _ from "lodash";

const RestaurantsList = props => {
  const restaurants = useSelector(store => store.firesore.ordered.restaurants);

  const renderRestaurants = () => {
    return _.chunk(restaurants, 3).map((group, index) => (
      <Row key={index}>
        {group.map((r, rIndex) => (
          <Col sm={4} key={`${index}=${rIndex}`}>
            <RestaurantCard
              data={r}
              onOpenForm={data => props.onOpenForm(data)}
            />
          </Col>
        ))}
      </Row>
    ));
  };

  if (!restaurants) {
    return (
      <h5
        style={{
          paddingLeft: 40
        }}
      >
        Loading...
      </h5>
    );
  }

  return <Container>{renderRestaurants()}</Container>;
};

export default RestaurantsList;
