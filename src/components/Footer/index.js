import React from "react";
import { SocialMediaIconsReact } from "social-media-icons-react";

const Footer = () => {
  return (
    <footer className="footer-distributed">
      <div className="footer-right">
        <a href="#" style={{ marginRight: 5, marginLeft: 5 }}>
          <i>
            <SocialMediaIconsReact
              icon="facebook"
              url=""
              size={35}
              iconSize={5}
              backgroundColor={"rgba(144, 225, 30, 0.8)"}
            />
          </i>
        </a>
        <a href="#" style={{ marginRight: 5, marginLeft: 5 }}>
          <i>
            <SocialMediaIconsReact
              icon="twitter"
              url=""
              size={35}
              iconSize={5}
              backgroundColor={"rgba(144, 225, 30, 0.8)"}
            />
          </i>
        </a>
        <a href="#" style={{ marginRight: 5, marginLeft: 5 }}>
          <i>
            <SocialMediaIconsReact
              icon="youtube"
              url=""
              size={35}
              iconSize={5}
              backgroundColor={"rgba(144, 225, 30, 0.8)"}
            />
          </i>
        </a>
        <a href="#" style={{ marginRight: 5, marginLeft: 5 }}>
          <i>
            <SocialMediaIconsReact
              icon="instagram"
              url=""
              size={35}
              iconSize={5}
              backgroundColor={"rgba(144, 225, 30, 0.8)"}
            />
          </i>
        </a>
      </div>

      <div className="footer-left">
        <p className="footer-links">
          <a className="link-1" href="#">
            Home
          </a>

          <a href="#">Blog</a>

          <a href="#">Pricing</a>

          <a href="#">About</a>

          <a href="#">Faq</a>

          <a href="#">Contact</a>
          {localStorage.getItem("name") && (
            <button
              className="logout"
              onClick={() => {
                localStorage.removeItem("name");
                window.location.reload();
              }}
            >
              Logout
            </button>
          )}
        </p>

        <p>Company Name &copy; 2019</p>
      </div>
    </footer>
  );
};

export default Footer;
