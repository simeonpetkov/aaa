import React, { Component } from "react";

class PopupContainer extends Component {
  componentDidMount() {
    document.addEventListener("mousedown", this.handleClick, false);
    document.addEventListener("keydown", this.escapeClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClick, false);
    document.removeEventListener("keydown", this.escapeClick, false);
  }

  handleClick = e => {
    // close popup menu, if the user clicks outside it
    if (this.node.contains(e.target)) {
      return;
    }
    this.props.hidePopup();
  };

  escapeClick = event => {
    // close dropdown menu on escape press
    if (event.keyCode === 27) {
      this.props.hidePopup();
    }
  };

  render() {
    return (
      <div className="o-popup-container">
        <div
          className={this.props.formClassName}
          ref={n => {
            this.node = n;
          }}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default PopupContainer;
