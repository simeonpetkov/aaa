import React, { Component } from "react";
import { connect } from "react-redux";

import PopupContainer from "../PopupContainer/index";
import { Field, reduxForm } from "redux-form";
import closeSVG from "./close-icon.svg";

const Input = props => {
  const inputElement = (
    <div style={{ position: "relative" }}>
      <input
        className={
          props.meta
            ? props.meta.error
              ? `${props.styles} c-form-input--invalid`
              : props.styles
            : props.styles
        }
        {...props}
        {...props.input}
        type={props.type}
        name={props.name}
        value={props.value}
        onChange={props.onChange}
        pattern={props.pattern}
        onInvalid={props.onInvalid}
        min={props.min}
        max={props.max}
      />
      {props.meta
        ? props.meta.touched &&
          ((props.meta.error && (
            <p className="u-font-input">{props.meta.error}</p>
          )) ||
            (props.meta.warning && (
              <p className="u-font-input">{props.meta.warning}</p>
            )))
        : null}
    </div>
  );
  return <div>{inputElement}</div>;
};

const reduxFormInput = field => (
  <Input
    value={field.input.value}
    onChange={e => field.input.onChange(e.target.value)}
    {...field}
  />
);

export class RegisterFromPopup extends Component {
  render() {
    const { hidePopup } = this.props;

    return (
      <PopupContainer
        formClassName="c-popup c-popup--secondary"
        hidePopup={hidePopup}
      >
        <form className="u-center-column" onSubmit={this.props.handleSubmit}>
          <img
            src={closeSVG}
            alt="close-svg"
            className="c-popup__close"
            onClick={hidePopup}
          />
          <h2 className="c-popup__title">Register</h2>
          <Field
            className="c-form-input"
            name="names"
            type="text"
            component={reduxFormInput}
            placeholder="[Names]"
            required
          />
          <Field
            className="c-form-input"
            name="password"
            type="password"
            placeholder="[Password]"
            component={reduxFormInput}
            required
          />
          <button className="c-button--secondary" type="submit">
            Sign up
          </button>
        </form>
      </PopupContainer>
    );
  }
}

RegisterFromPopup = reduxForm({
  form: "register",
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(RegisterFromPopup);

RegisterFromPopup = connect(
  (state, props) => {
    return {};
  },
  dispatch => ({})
)(RegisterFromPopup);

export default RegisterFromPopup;
