import React, { Component } from "react";
import { connect } from "react-redux";

import PopupContainer from "../PopupContainer/index";
import { Field, reduxForm } from "redux-form";
import closeSVG from "./close-icon.svg";

const places = [
  "Inside - smoking is not allowed",
  "Outside - smoking is allowed",
  "Children's room"
];

const time = ["Morning", "Noon", "Afternoon", "Evening"];

const normalizePhone = value => {
  if (!value) {
    return value;
  }

  const onlyNums = value.replace(/[^\d]/g, "");
  if (onlyNums.length <= 3) {
    return onlyNums;
  }
  if (onlyNums.length <= 7) {
    return `${onlyNums.slice(0, 3)}-${onlyNums.slice(3)}`;
  }
  return `${onlyNums.slice(0, 3)}-${onlyNums.slice(3, 6)}-${onlyNums.slice(
    6,
    10
  )}`;
};

const Input = props => {
  const inputElement = (
    <div style={{ position: "relative" }}>
      <input
        className={
          props.meta
            ? props.meta.error
              ? `${props.styles} c-form-input--invalid`
              : props.styles
            : props.styles
        }
        {...props}
        {...props.input}
        type={props.type}
        name={props.name}
        value={props.value}
        onChange={props.onChange}
        pattern={props.pattern}
        onInvalid={props.onInvalid}
        min={props.min}
        max={props.max}
      />
      {props.meta
        ? props.meta.touched &&
          ((props.meta.error && (
            <p className="u-font-input">{props.meta.error}</p>
          )) ||
            (props.meta.warning && (
              <p className="u-font-input">{props.meta.warning}</p>
            )))
        : null}
    </div>
  );
  return <div>{inputElement}</div>;
};

const reduxFormInput = field => (
  <Input
    value={field.input.value}
    onChange={e => field.input.onChange(e.target.value)}
    {...field}
  />
);

export class ReserveFromPopup extends Component {
  render() {
    const { hidePopup } = this.props;

    return (
      <PopupContainer
        formClassName="c-popup c-popup--secondary"
        hidePopup={() => {
          hidePopup();
        }}
      >
        <form className="u-center-column" onSubmit={this.props.handleSubmit}>
          <img
            src={closeSVG}
            alt="close-svg"
            className="c-popup__close"
            onClick={hidePopup}
          />
          <h2 className="c-popup__title">Make your reservation</h2>
          <Field
            className="c-form-input"
            name="names"
            type="text"
            component={reduxFormInput}
            placeholder="[Names]"
            required
          />
          <Field
            className="c-form-input"
            name="date"
            type="date"
            component={reduxFormInput}
            required
          />
          <Field
            className="c-form-input"
            name="phone"
            type="text"
            component={reduxFormInput}
            placeholder="[Phone number]"
            normalize={normalizePhone}
            required
          />
          <Field
            name="time"
            component="select"
            className="c-form-input"
            required
          >
            <option value="" disabled defaultValue hidden>
              [Time]
            </option>
            {time.map(p => (
              <option key={p} value={p}>
                {p}
              </option>
            ))}
          </Field>
          <Field
            name="place"
            component="select"
            className="c-form-input"
            required
          >
            <option value="" disabled defaultValue hidden>
              [Table location]
            </option>
            {places.map(p => (
              <option key={p} value={p}>
                {p}
              </option>
            ))}
          </Field>
          <button className="c-button--secondary" type="submit">
            Reserve
          </button>
        </form>
      </PopupContainer>
    );
  }
}

ReserveFromPopup = reduxForm({
  form: "addReservation",
  enableReinitialize: true,
  keepDirtyOnReinitialize: true
})(ReserveFromPopup);

ReserveFromPopup = connect(
  (state, props) => {
    return {
      initialValues: {
        names: props.user ? props.user.names : "",
        phone: props.user ? props.user.phone : "",
        place: props.user ? props.user.place : "",
        time: props.user ? props.user.time : ""
      }
    };
  },
  dispatch => ({})
)(ReserveFromPopup);

export default ReserveFromPopup;
