import React, { useState, useEffect } from "react";
import { NavLink, Link } from "react-router-dom";

const Navigation = props => {
  const { currentPath, name } = props;

  const [hasName, setName] = useState(name);

  useEffect(() => {
    setName(name);
  }, [name]);

  const isHomeScreen = currentPath === "/";
  return (
    <div className="c-navigation">
      {localStorage.getItem("name") && (
        <Link to={isHomeScreen ? "/reservations" : "/"}>
          <button className="c-button--primary">
            {isHomeScreen ? "History" : "Back"}
          </button>
        </Link>
      )}
    </div>
  );
};

export default Navigation;
