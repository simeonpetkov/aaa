import React, { useState, useEffect } from "react";
import logo from "../../icons/logo.png";
import Navigation from "./navigation";
import Background from "../../images/background-image.jpg";
import { useWindowDimensions } from "./WindowDimensions/index";
import Arrow from "./arrow.svg";
import { withRouter } from "react-router-dom";
import Search from "../Search/index";

const Header = props => {
  const { onScrollDown, location, onOpenForm, name } = props;
  const { height } = useWindowDimensions();
  const currentPath = location.pathname;
  const isHomeScreen = currentPath === "/";
  const [hasName, setName] = useState(name);

  useEffect(() => {
    setName(name);
  }, [name]);
  return (
    <div
      className="c-header-container"
      style={{
        backgroundImage: `url(${Background})`,
        height,
        marginBottom: isHomeScreen ? "42px" : 0
      }}
    >
      <div className="c-header-container__bar">
        <img className="c-logo--small" src={logo} alt="header-logo" />
        <Navigation currentPath={currentPath} name={hasName} />
      </div>
      {isHomeScreen && (
        <div className="c-slogan">
          {name !== "" && name !== undefined && name !== null
            ? `Hi ${name},\nreserve with style`
            : "Reserve with style"}
        </div>
      )}
      {isHomeScreen && (
        <div className="o-search-wrapper">
          <div style={{ position: "relative" }}>
            <Search onOpenForm={suggestion => onOpenForm(suggestion)} />
          </div>
        </div>
      )}
      {isHomeScreen && (
        <button
          className="c-button--primary scroll-down-button"
          style={{ width: 78, height: 36 }}
          onClick={onScrollDown}
        >
          <div>
            <img src={Arrow} className="rotate-90" />
          </div>
        </button>
      )}
    </div>
  );
};

export default withRouter(Header);
