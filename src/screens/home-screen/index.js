import React from "react";
import { connect } from "react-redux";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import RestaurantsList from "../../components/RestaurantsList/index";
import ScrollButton from "react-scroll-up-button";
import { ScrollTo } from "react-scroll-to";
import PopupContainer from "../../components/PopupContainer/index";

import ReservePopup from "../../components/ReserveForm/index";
import RegisterPopup from "../../components/RegisterForm/index";

import { firestoreConnect } from "react-redux-firebase";
import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css

import "firebase/database";

const Input = props => {
  const inputElement = (
    <div style={{ position: "relative" }}>
      <input
        className={
          props.meta
            ? props.meta.error
              ? `${props.styles} c-form-input--invalid`
              : props.styles
            : props.styles
        }
        {...props}
        {...props.input}
        type={props.type}
        name={props.name}
        value={props.value}
        onChange={props.onChange}
        pattern={props.pattern}
        onInvalid={props.onInvalid}
        min={props.min}
        max={props.max}
      />
      {props.meta
        ? props.meta.touched &&
          ((props.meta.error && (
            <p className="u-font-input">{props.meta.error}</p>
          )) ||
            (props.meta.warning && (
              <p className="u-font-input">{props.meta.warning}</p>
            )))
        : null}
    </div>
  );
  return <div>{inputElement}</div>;
};

const addReservation = (reservationData, currentRestaurant) => (
  dispatch,
  getState,
  db
) => {
  db.collection("reservations")
    .where("name", "==", currentRestaurant.name)
    .get()
    .then(querySnapshot => {
      if (querySnapshot.docs.length) {
        querySnapshot.forEach(function(doc) {
          db.collection("reservations")
            .doc(doc.id)
            .update({
              date: reservationData.date,
              place: reservationData.place,
              time: reservationData.time,
              names: reservationData.names,
              phone: reservationData.phone
            });
        });
      } else {
        db.collection("reservations").add({
          ...reservationData
        });
      }
    });
};

class HomeScreen extends React.Component {
  state = {
    restaurants: [],
    popupVisible: false,
    currentRestaurant: {},
    openRegister: false,
    name: ""
  };

  static getDerivedStateFromProps(props) {
    return {
      restaurants: props.restaurants || []
    };
  }

  openConfirm = () => {
    this.onHidePopup();
    this.submit();
  };

  onSubmitHandler = values => {
    const { onAddReservation } = this.props;
    const { currentRestaurant } = this.state;
    onAddReservation({ ...values, ...currentRestaurant }, currentRestaurant);
    this.onHidePopup();
  };

  onShowPopup = () => {
    this.setState({ popupVisible: true });
  };

  onHidePopup = () => {
    this.setState({ popupVisible: false, currentRestaurant: {} });
  };

  onOpenForm = data => {
    this.setState({ currentRestaurant: data });
    this.onShowPopup();
  };

  openRegister = () => {
    this.setState({ openRegister: true });
  };

  submit = data => {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <PopupContainer
            formClassName="c-confirm"
            hidePopup={() => {
              onClose();
            }}
          >
            <div className="custom-ui">
              <h5>Do you want to register?</h5>
              <p>Create an account and get full access to our features!</p>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  paddingTop: 10
                }}
              >
                <button
                  className="no"
                  onClick={() => {
                    onClose();
                  }}
                >
                  No, just complete reservation
                </button>
                <button
                  className="yes"
                  onClick={() => {
                    onClose();
                    this.openRegister();
                  }}
                >
                  Yes, create a profile
                </button>
              </div>
            </div>
          </PopupContainer>
        );
      }
    });
  };

  render() {
    const { openRegister } = this.state;
    return (
      <ScrollTo className="c-main-container">
        {({ scroll }) => (
          <div>
            <Header
              onScrollDown={() =>
                scroll({ x: 0, y: window.innerHeight, smooth: true })
              }
              name={localStorage.getItem("name")}
              onOpenForm={suggestion => this.onOpenForm(suggestion)}
            />
            {!openRegister && <RestaurantsList onOpenForm={this.onOpenForm} />}
            {this.state.restaurants.length !== 0 && <Footer />}
            <ScrollButton
              ToggledStyle={{ borderRadius: 5, outline: "none" }}
              style={{ borderRadius: 5, outline: "none" }}
            />
            {this.state.popupVisible && (
              <ReservePopup
                hidePopup={this.onHidePopup}
                onSubmit={values => {
                  this.onSubmitHandler(values);
                  if (this.state.name === null || this.state.name === "") {
                    this.openConfirm();
                  }
                }}
              />
            )}
            {openRegister && (
              <RegisterPopup
                hidePopup={() => {
                  this.setState({ openRegister: false });
                }}
                onSubmit={values => {
                  this.setState({
                    openRegister: false,
                    name: values.names.split(" ")[0]
                  });
                  localStorage.setItem("name", values.names.split(" ")[0]);
                }}
              />
            )}
          </div>
        )}
      </ScrollTo>
    );
  }
}

const mapStateToProps = state => ({
  restaurants: state.firesore.ordered.restaurants
});

const mapDispatchToProps = dispatch => ({
  onAddReservation: (reservationData, currentRestaurant) =>
    dispatch(addReservation(reservationData, currentRestaurant))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(firestoreConnect([{ collection: "restaurants" }])(HomeScreen));
