import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import Header from "../../components/Header";
import { Container, Row, Col } from "react-grid-system";
import RestaurantCard from "../../components/RestaurantCard/index";
import _ from "lodash";
import ReservePopup from "../../components/ReserveForm/index";

import "firebase/database";

const Reservations = props => {
  const dispatch = useDispatch();
  const reservations = useSelector(
    store => store.firesore.ordered.reservations
  );

  const addReservation = reservationData => (dispatch, getState, db) => {
    db.collection("reservations")
      .where("name", "==", currentRestaurant.name)
      .get()
      .then(querySnapshot => {
        if (querySnapshot.docs.length) {
          querySnapshot.forEach(function(doc) {
            db.collection("reservations")
              .doc(doc.id)
              .update({
                date: reservationData.date,
                place: reservationData.place,
                time: reservationData.time,
                names: reservationData.names,
                phone: reservationData.phone
              });
          });
        } else {
          db.collection("reservations").add({
            ...reservationData
          });
        }
      });
  };

  const [popupVisible, setPopupVisible] = useState(false);
  const [currentUser, setCurrentUser] = useState({});
  const [currentRestaurant, setCurrentRestaurant] = useState({});

  const onSubmitHandler = values => {
    dispatch(addReservation({ ...currentRestaurant, ...values }));
    onHidePopup();
  };

  const onShowPopup = () => {
    setPopupVisible(true);
  };

  const onHidePopup = () => {
    setPopupVisible(false);
    setCurrentUser({});
    setCurrentRestaurant({});
  };

  const onOpenForm = (u, r) => {
    setCurrentUser(u);
    setCurrentRestaurant(r);
    onShowPopup();
  };

  const renderReservations = () => {
    return _.chunk(reservations, 2).map((group, index) => (
      <Row key={index}>
        {group.map((r, rIndex) => (
          <Col sm={6} key={`${index}=${rIndex}`}>
            <RestaurantCard
              data={r}
              user={r}
              onOpenForm={data => onOpenForm(r, data)}
            />
          </Col>
        ))}
      </Row>
    ));
  };

  return (
    <div>
      <Header />
      <div className="c-reservations-container">
        <p className="title">Your previous reservations</p>
        <div
          style={{
            position: "relative"
          }}
        >
          {reservations !== undefined && (
            <Container>{renderReservations()}</Container>
          )}
        </div>
      </div>
      {popupVisible && (
        <ReservePopup
          hidePopup={onHidePopup}
          onSubmit={onSubmitHandler}
          user={currentUser}
        />
      )}
    </div>
  );
};

export default firestoreConnect([{ collection: "reservations" }])(Reservations);
