import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import HomeScreen from "./screens/home-screen/index";
import Reservations from "./screens/reservations/index";

import "./App.scss";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <Switch>
              <Route path="/" exact component={HomeScreen} />
              <Route path="/reservations" exact component={Reservations} />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
